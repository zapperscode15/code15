﻿CREATE TABLE [dbo].[EducationType] (
    [Id]          INT           IDENTITY (1, 1) NOT NULL,
    [Description] VARCHAR (100) NOT NULL,
    [Code]        NCHAR (50)    NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [AK_EducationType_Description] UNIQUE NONCLUSTERED ([Description] ASC),
    CONSTRAINT [AK_EducationType_Code] UNIQUE NONCLUSTERED ([Code] ASC)
);

