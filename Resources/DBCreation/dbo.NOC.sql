﻿CREATE TABLE [dbo].[NOC] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Code]        NCHAR (10)     NOT NULL,
    [Description] NVARCHAR (500) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [AK_NOC_Code] UNIQUE NONCLUSTERED ([Code] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_NOC_Code]
    ON [dbo].[NOC]([Code] ASC);

