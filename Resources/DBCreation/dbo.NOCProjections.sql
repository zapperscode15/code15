﻿CREATE TABLE [dbo].[NOCProjections] (
    [Id]                                            INT            IDENTITY (1, 1) NOT NULL,
    [Employment_2012]                               FLOAT (53)     NULL,
    [Cumiliative_Emp_Growth_2013-2022]              FLOAT (53)     NULL,
    [Cumulative_Retirements_2013-2022]              FLOAT (53)     NULL,
    [Cumulative_Other_Replacement_Demand_2013-2022] FLOAT (53)     NULL,
    [Cumulative_JobOpenings_2013-2022 ]             FLOAT (53)     NULL,
    [Cumulative_School_Leavers_2013-2022 ]          FLOAT (53)     NULL,
    [Cumulative_Immigrants_In_Market_2013-2022]     FLOAT (53)     NULL,
    [Cumulative_Jobseekers_Other_2013-2022 ]        FLOAT (53)     NULL,
    [Cumulative _Jobseekers_2013-2022]              FLOAT (53)     NULL,
    [Assessment_Labourmarket_2010-2012_Eng]         NVARCHAR (100) NULL,
    [Assessment_Labourmarket_2010-2012_Fr]          NVARCHAR (100) NULL,
    [Final_Assessment_Eng]                          NVARCHAR (100) NULL,
    [Final_Assessment_Fr ]                          NVARCHAR (100) NULL,
    [NOC_CODE]                                      NCHAR (10)     NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

