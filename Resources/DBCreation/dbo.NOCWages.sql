﻿CREATE TABLE [dbo].[NOCWages] (
    [Id]                  INT          IDENTITY (1, 1) NOT NULL,
    [NOC_CODE]            NCHAR (10)   NOT NULL,
    [Education_Type]      INT          NOT NULL,
    [EmploymentIncome]    DECIMAL (18) NULL,
    [MedianEmployment]    DECIMAL (18) NULL,
    [AvgEmployment]       DECIMAL (18) NULL,
    [WageNSalaries]       DECIMAL (18) NULL,
    [MedianWageNSalaries] DECIMAL (18) NULL,
    [AvgWageNSalaries]    DECIMAL (18) NULL,
    [ProvinceCode]        NCHAR (3)    NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

