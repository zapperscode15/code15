﻿CREATE TABLE [dbo].[Provinces] (
    [Id]           INT           IDENTITY (1, 1) NOT NULL,
    [ProvinceCode] NCHAR (3)     NOT NULL,
    [ProvinceName] NVARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [AK_Province_Code] UNIQUE NONCLUSTERED ([ProvinceCode] ASC),
    CONSTRAINT [CK_Provinces_Column] CHECK ((1)=(1))
);


GO
CREATE NONCLUSTERED INDEX [IX_Provinces_ProvinceCode]
    ON [dbo].[Provinces]([ProvinceCode] ASC);

