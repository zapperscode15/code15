SET IDENTITY_INSERT [dbo].[EducationType] ON
INSERT INTO [dbo].[EducationType] ([Id], [Description], [Code]) VALUES (24, N'No certificate, diploma or degree', N'NO_CERT                                           ')
INSERT INTO [dbo].[EducationType] ([Id], [Description], [Code]) VALUES (30, N'High school diploma or equivalent', N'HIGHSCHOOL_EQU                                    ')
INSERT INTO [dbo].[EducationType] ([Id], [Description], [Code]) VALUES (31, N'Postsecondary certificate, diploma or degree', N'POST_SECONDARY                                    ')
INSERT INTO [dbo].[EducationType] ([Id], [Description], [Code]) VALUES (34, N'Postsecondary certificate or diploma below bachelor level', N'POST_SECONDAR_BELOW_BACHELORS                     ')
INSERT INTO [dbo].[EducationType] ([Id], [Description], [Code]) VALUES (35, N'University certificate, diploma or degree at bachelor level or above', N'UNIVERSITY_CERTIFICATE                            ')
SET IDENTITY_INSERT [dbo].[EducationType] OFF
