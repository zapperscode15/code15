Application description

Name: RightCareer

Description:
Our application in a web application that helps people choose the right job based on wages and future vacancy projections for positions in National Occupational Classification.

Based on user input choosing a degree type and province , our algorithm initially returns the best future job available. The algorithm decision is based on the future vacancy projected for a job and the average wage for that position in the province.

Example: A position with 15 projected vacancies and 40000 average salary will have greater precedence over a position with 10 projected vacancies and 50000 average salary. 

After the initial list users can sort the career list by position description, median wage , average wage with in the province.

Clicking on each item in list will display further stats about the average wage, median wage and out algorithm ranking.

Note:
The wages are base on 2010 wages.

The projection are based on projected vacancies between 2013 and 2022.

Datasets used
