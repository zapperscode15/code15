<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="schema.dcterms" href="http://purl.org/dc/terms/" />
    <meta name="dcterms.language" scheme="ISO639-2" content="eng" /> 
	<!-- Web Experience Toolkit (WET) / Bo�te � outils de l'exp�rience Web (BOEW)
	Terms and conditions of use: http://tbs-sct.ircan.gc.ca/projects/gcwwwtemplates/wiki/Terms
	Conditions r�gissant l'utilisation : http://tbs-sct.ircan.gc.ca/projects/gcwwwtemplates/wiki/Conditions
	-->
	<!-- WET scripts/CSS begin / D�but des scripts/CSS de la BOEW --><!--[if IE 6]><![endif]-->
	<link href="/iadtmpltv4/internet-rhdcc-hrsdc/css/base.css" rel="stylesheet" type="text/css" />
	<!--[if IE 8]><link href="/iadtmpltv4/internet-rhdcc-hrsdc/css/base-ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
	<!--[if IE 7]><link href="/iadtmpltv4/internet-rhdcc-hrsdc/css/base-ie7.css" rel="stylesheet" type="text/css" /><![endif]-->
	<!--[if lte IE 6]><link href="/iadtmpltv4/internet-rhdcc-hrsdc/css/base-ie6.css" rel="stylesheet" type="text/css" /><![endif]-->
	<link href="/iadtmpltv4/internet-rhdcc-hrsdc/css/util.css" rel="stylesheet" type="text/css" />
<!-- clf2-nsi2 theme begins / D�but du th�me clf2-nsi2 -->
	<link href="/iadtmpltv4/internet-rhdcc-hrsdc/css/theme-clf2-nsi2/theme-clf2-nsi2.css" rel="stylesheet" type="text/css" />
	<!--[if IE 7]><link href="/iadtmpltv4/internet-rhdcc-hrsdc/css/theme-clf2-nsi2/theme-clf2-nsi2-ie7.css" rel="stylesheet" type="text/css" /><![endif]-->
	<!--[if lte IE 6]><link href="/iadtmpltv4/internet-rhdcc-hrsdc/css/theme-clf2-nsi2/theme-clf2-nsi2-ie6.css" rel="stylesheet" type="text/css" /><![endif]-->
<!-- clf2-nsi2 theme ends / Fin du th�me clf2-nsi2 -->
  <!-- include_archivedcss -->
	<!-- WET scripts/CSS end / Fin des scripts/CSS de la BOEW -->
	<link type="text/css" href="/iadtmpltv4/jquery/css/smoothness/jquery-ui-1.8.10.custom.css" rel="Stylesheet" />	
	<!--[if IE]><link type="text/css" href="/iadtmpltv4/jquery/css/smoothness/jquery-ui-1.8.10.custom.ie.css" rel="Stylesheet" /><![endif]-->
	<!-- Progressive enhancement begins / D�but de l'am�lioration progressive -->
	<script src="/iadtmpltv4/jquery/js/jquery-1.5.1.min.js" type="text/javascript"></script>
	<script src="/iadtmpltv4/jquery/js/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	<!--[if lte IE 6]><script src="/iadtmpltv4/jquery/js/jquery.bgiframe.js" type="text/javascript"></script><![endif]-->
	<script type="text/javascript">
		var appuserlanguage = "en";
		var appurl = "http://www23.rhdcc-hrsdc.gc.ca";
		var appisprinterfriendlyind = "2";
		var appservletalias = "/servlet/copspub";
		var appajaxenabledind = "<!-- include_ajaxenabledind -->";
	  	var appblockformats = "p,h1,h2,h3,h4";
		var appuseruploadrelativedir = "/images";
		var appcontenteditinglang = "";
		var appisuserloggedonind = false;
	    var appjssubmittarget = "dataframe";
	</script>
	<script src="/iadtmpltv4/boew-wet-utils/js/pe-ap.js" type="text/javascript" id="progressive"></script>
	<script type="text/javascript">
	/* <![CDATA[ */
		var params = {
		};
		PE.progress(params);
	/* ]]> */
	</script>
	<!-- Progressive enhancement ends / Fin de l'am�lioration progressive -->
	<!-- Custom scripts/CSS begin / D�but des scripts/CSS personnalis�s -->
	<link href="/iadtmpltv4/internet-rhdcc-hrsdc/css/theme-hrsdc-rhdcc-internet/theme-hrsdc-rhdcc-internet.css" rel="stylesheet" type="text/css" />
	<!--[if IE 8]><link href="/iadtmpltv4/internet-rhdcc-hrsdc/css/theme-hrsdc-rhdcc-internet/theme-hrsdc-rhdcc-internet-ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
	<!--[if IE 7]><link href="/iadtmpltv4/internet-rhdcc-hrsdc/css/theme-hrsdc-rhdcc-internet/theme-hrsdc-rhdcc-internet-ie7.css" rel="stylesheet" type="text/css" /><![endif]-->
	<!--[if lte IE 6]><link href="/iadtmpltv4/internet-rhdcc-hrsdc/css/theme-hrsdc-rhdcc-internet/theme-hrsdc-rhdcc-internet-ie6.css" rel="stylesheet" type="text/css" /><![endif]-->
	<link href="/iadtmpltv4/iad/css/iad.css" media="screen, print" rel="stylesheet" type="text/css" />
	<link href="/iadtmpltv4/internet-rhdcc-hrsdc/css/theme-hrsdc-rhdcc-internet/pf-if-theme-hrsdc-rhdcc-internet.css" rel="stylesheet" type="text/css" />
	<!-- Custom scripts/CSS end / Fin des scripts/CSS personnalis�s -->
	<!-- WET print CSS begins / D�but du CSS de la BOEW pour l'impression -->
	<link href="/iadtmpltv4/internet-rhdcc-hrsdc/css/pf-if.css" rel="stylesheet" media="print" type="text/css" />
<!-- clf2-nsi2 theme begins / D�but du th�me clf2-nsi2 -->
	<link href="/iadtmpltv4/internet-rhdcc-hrsdc/css/theme-clf2-nsi2/pf-if-theme-clf2-nsi2.css" rel="stylesheet" media="print" type="text/css" />
<!-- clf2-nsi2 theme ends / Fin du th�me clf2-nsi2 -->
	<!-- WET print CSS ends / Fin du CSS de la BOEW pour l'impression -->
	<!-- include_javascript -->
	<script type="text/javascript" src="/iadtmpltv4/iad/js/iad.js"></script>
	<!--
	<meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" /> 
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Pragma" content="no-cache" /> 
	-->
	<!--part1 end-->
<title>Glossary / Canadian Occupational Projection System (COPS)</title>
<meta name="description" content="Glossary / Canadian Occupational Projection System (COPS)" />
<meta name="dcterms.title" content="Glossary / Canadian Occupational Projection System (COPS)" />
<meta name="dcterms.description" content="Glossary / Canadian Occupational Projection System (COPS)" />
<meta name="dcterms.language" content="eng" />
<meta name="dcterms.creator" 
  content="Minist&egrave;re Ressources humaines et D&eacute;veloppement social Canada" />
<meta name="dcterms.subject" 
  content="Apprenticeships, Training programs, Retraining, Vocational Training, Certification" />
<meta name="dcterms.modified" content="2011-02-28" />
<meta name="dcterms.created" content="2011-02-28" />
<link rel="stylesheet" type="text/css" href="css/copscss.css" />
<script type="text/javascript" src="js/cops.js"></script>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-13011273-15']);
  _gaq.push (['_gat._anonymizeIp']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<!-- part 2 starts -->
</head>
<body>
<!-- Three column layout begins / D�but de la mise en page de trois colonnes -->
<div id="cn-body-inner-2col">
	<!-- Skip header begins / D�but du saut de l'en-t�te -->
	<div id="cn-skip-head">
		<ul id="cn-tphp">
			<li id="cn-sh-link-1"><a href="#cn-cont">Skip to main content</a></li>
			<li id="cn-sh-link-2"><a href="#cn-nav">Skip to primary navigation</a></li>
		</ul>
	</div>
	<!-- Skip header ends / Fin du saut de l'en-t�te -->
	<!-- Header begins / D�but de l'en-t�te -->
	<div id="cn-head"><div id="cn-head-inner">
<!-- clf2-nsi2 theme begins / D�but du th�me clf2-nsi2 -->
		<div id="cn-sig"><img src="/iadtmpltv4/internet-rhdcc-hrsdc/img/sig-eng.gif" height="25" alt="Human Resources and Skills Development Canada" /></div>
		<div id="cn-wmms"><img src="/iadtmpltv4/internet-rhdcc-hrsdc/img/wmms.gif" width="83" height="20" alt="Symbol of the Government of Canada" /></div>
		<!-- Banner begins / D�but de la banni�re -->
		<div id="cn-leaf"></div>
		<div id="cn-banner" class="cn-banner-hrsdc">
			<p id="cn-banner-text">Employment and Social Development Canada</p>
			<p>www.hrsdc.gc.ca</p>
		</div>
		<!-- Banner ends / Fin de la banni�re -->
			<!-- Common menu bar begins / D�but de la barre de menu commune -->
			<div id="cn-cmb">
				<h2>Common menu bar</h2>
				<ul>
					<li id="cn-cmb1"><a href="http://www23.rhdcc.gc.ca/gl.4ss.1ry@-fra.jsp" lang="fr" xml:lang="fr" title="Fran�ais - Version fran�aise de cette page">Fran�ais</a></li>
					<li id="cn-cmb2"><a href="http://www.hrsdc.gc.ca/eng/home.shtml" title="Home - The main page of the Employment and Social Development Canada Website">Home</a></li>
					<li id="cn-cmb3"><a href="c.4nt.1ct.5s@-eng.jsp" title="Contact Us - Contact Employment and Social Development Canada">Contact Us</a></li>
					<li id="cn-cmb4"><a href="h.2lp@-eng.jsp?lang=eng" title="Help - Information on how to use the Website of Employment and Social Development Canada">Help</a></li>
					<li id="cn-cmb5"><a rel="search" href=".4cc.5p.1t.3onalforcastsummarys.2arch@-eng.jsp?lang=eng" title="Search - Search the Employment and Social Development Canada Website">Search</a></li>
					<li id="cn-cmb6"><a rel="external" href="http://www.canada.gc.ca/home.html" title="canada.gc.ca - Government of Canada Web site">canada.gc.ca</a></li>
				</ul>
			</div>
			<!-- Common menu bar ends / Fin de la barre de menu commune -->
<!-- part 2 ends -->
<!-- auto generated breadCrumb start -->
<div id="cn-bcrumb"><h2>Breadcrumb</h2><ol><li><a href="http://www.hrsdc.gc.ca/eng/home.shtml" >Home</a>&#160;&#62;</li>
<li><a href="w.2lc.4m.2@-eng.jsp" ><acronym title="Canadian Occupational Projection System">COPS</acronym> Home</a>&#160;&#62;</li>
<li >Glossary</li></ol></div><!-- auto generated breadCrumb end -->
<!-- auto generated abandonchanges start -->
<input type="hidden" name="abandonchangesjsp" value="welcome.jsp?fbc=Y" />
<!-- auto generated abandonchanges end -->
<!-- part 3 starts -->
<!-- clf2-nsi2 theme ends / Fin du th�me clf2-nsi2 -->
	</div></div>
	<!-- Header ends / Fin de l'en-t�te -->
	<div id="cn-cols">
	<!-- Main content begins / D�but du contenu principal --> 
	<div id="cn-centre-col-gap"></div>
	<div id="cn-centre-col"><div id="cn-centre-col-inner">
<!-- part 3 ends -->
<h1 id="cn-cont">Canadian Occupational Projection System (<acronym title="Canadian Occupational Projection System">COPS</acronym>)</h1>
<h2>Glossary</h2>	
	<dl><dt><strong>Chained (2002) Dollars </strong></dt><dd>specifies that the values are in 2002 dollars and they are adjusted for inflation.</dd><dt><strong>Canadian Occupational Projection System (COPS) </strong>is a<strong> </strong></dt><dd>set of economic models used to assess and project future labour market conditions on an industrial and occupational basis.</dd><dt><strong>Demographic</strong></dt><dd>is a statistic characterizing human populations (or segments of human populations broken down by age or sex etc.)</dd><dt><strong>Death </strong></dt><dd>(in-service mortality) refers to a withdrawal from the labour market due to mortality.<strong>&nbsp;</strong></dd><dt><strong>Economic growth</strong></dt><dd>is the change in real gross domestic product (GDP).</dd><dt><strong>Employment </strong></dt><dd>refers to the number of persons 15 years of age and over who had a job during a given year.</dd><dt><strong>Employment rate</strong></dt><dd>is the number of persons employed expressed as a percentage of the population 15 years of age and over. The employment rate for a particular group (age, sex, marital status, etc.) is the number employed in that group expressed as a percentage of the population for that group.</dd><dt><strong>Excess Demand</strong></dt><dd>occurs when there are more job openings than job seekers.</dd><dt><strong>Expansion Demand</strong></dt><dd>refers to the annual creation or destruction of jobs as a result of economic growth.</dd><dt><strong>Gross domestic product (GDP)</strong></dt><dd>is the market value of final goods and services produced in an economy in a given year.</dd><dt><strong>High-skilled occupations</strong></dt><dd>include i) occupations usually requiring university education, ii) college education or apprenticeship training and iii) management occupations (which do not always require postsecondary education)</dd><dt><strong>Inflation</strong></dt><dd>is the percentage change in prices from one period to the next.</dd><dt><strong>Job openings</strong></dt><dd>are the number of new jobs due to changing economic activity and positions becoming vacant because of death, retirement, occupational mobility, and temporary labour force withdrawal.</dd><dt><strong>Labour force</strong></dt><dd>is the number of persons 15 years of age and over that were working or are looking for work in a given year. In other words, it refers to those who were employed or unemployed. If a person is not working and is not actively looking for work then that person is not included in the labour force.</dd><dt><strong>Labour mobility </strong></dt><dd>is the ability of workers to move between geographical locations, among industries and different occupational skills.</dd><dt><strong>Labour supply </strong></dt><dd>refers to the number of individuals offering their services to employers, including new entrants to the labour market as well as those with or without a job that are actively looking for work.</dd><dt><strong>Low-skilled occupations</strong></dt><dd>are a set of occupations that usually require secondary school or occupation-specific training or only on-the-job training.</dd><dt><strong>National graduate survey - </strong><strong>NGS</strong></dt><dd>is a Statistics Canada survey in which postsecondary graduates are surveyed two and five years after graduation about the link between their education characteristics and their labour market status.</dd><dt><strong>National Occupational Classification - </strong><strong>NOC</strong></dt><dd>refers to a standard that classifies and describes the occupations in the Canadian economy. For more information visit: <a href="http://www5.hrsdc.gc.ca/NOC/English/NOC/2006/Welcome.aspx">http://www5.hrsdc.gc.ca/NOC/English/NOC/2006/Welcome.aspx</a></dd><dt><strong>NFLMS </strong></dt><dd>stands for Normalized Future Labour Market Situation. It's an indicator of excess demand (excess supply if negative) normalized to the base year employment.</dd><dt><strong>NGS</strong><strong> -</strong></dt><dd>See national graduate survey - NGS (enqu�te nationale aupr�s des dipl�m�s - END).</dd><dt><strong>NOC</strong><strong> - </strong></dt><dd>See National Occupational Classification - NOC (Classification nationale des professions ? CNP).</dd><dd><br /></dd><dt><strong>North American Industry Classification System NAICS </strong></dt><dd>is an industry classification system developed by the statistical agencies of Canada, Mexico and the United States. For more information visit the Standard Industry Classification site from Statistics Canada: <a href="http://www.statcan.gc.ca/concepts/industry-industrie-eng.htm">http://www.statcan.gc.ca/concepts/industry-industrie-eng.htm</a></dd><dt><strong>Occupational group</strong></dt><dd>refers to a collection of similar occupations found in various industries or organizations. An occupational group consists of a 2- or 3-digit NOC codes is a combined grouping of two or more occupations of which are at the 4-digit NOC code level.</dd><dt><strong>(Net) Occupational Mobility</strong></dt><dd>captures individuals currently in the labour force moving between occupations, takes two forms:</dd><dd>
<ul>
<li>Vertical labour mobility, in which workers move between occupations that require a different skill level. This includes upward occupational mobility, as workers who have gained labour force experience move to management positions, and downward occupational mobility, where workers choose to enter low-skilled occupations as part of their transition towards retirement. </li>
<li>Horizontal labour mobility, in which workers move between occupations within the same skill level.</li>
</ul>
</dd><dt><strong>Participation rate </strong></dt><dd>is the number of labour force participants expressed as a percentage of the population 15 years of age and over. The participation rate for a particular group (age, sex, marital status, etc.) is the number of labour force participants in that group expressed as a percentage of the population for that group.</dd><dt><strong>Population </strong>is<strong> </strong></dt><dd>the number of persons of working age, 15 years of age and over.<strong> </strong></dd><dt><strong>Post-secondary </strong></dt><dd>is a continuance of some level of education after completion of high school.</dd><dt><strong>Profession </strong></dt><dd>is an occupation that requires specialized skills and advanced training.</dd><dt><strong>Projections </strong>refer to a<strong> </strong></dt><dd>numerically-based view of the future economy built upon past data, computer models, expert knowledge and consultations.</dd><dt><strong>Real Gross Domestic Product</strong></dt><dd>(Real GDP) is value of all final goods and services produced in a geographical region, adjusted for inflation. &nbsp;</dd><dt><strong>Reference week </strong></dt><dd>is the week of the month in which the Labour Force Survey is conducted by Statistics Canada. It is the week that contains the 15th day of the month. <em>Source: Statistics </em><em>Canada</em><em>: Guide to the Labour Force Survey - 2008</em></dd><dt><strong>Replacement Demand</strong></dt><dd>refers to the job openings resulting from retirements, emigration and in-job mortality.</dd><dt><strong>Retirement </strong></dt><dd>is a complete and permanent withdrawal from the labour market, excluding migration and those workers that die while holding a job.</dd><dt><strong>Retirement rate </strong></dt><dd>is the ratio of retirements to workers of a given occupation, occupational group, industry and/or other.</dd><dt><strong>School Leavers</strong></dt><dd>are students coming out of Canada's education system, with educational attainment ranging from an incomplete high school to a PhD</dd><dt><strong>Skill </strong></dt><dd>refers to the learned capacity or ability, to perform a task or job.</dd><dt><strong>Skill lev</strong><strong>el</strong><strong> </strong></dt><dd>is primarily based on the nature of education and training required to work in an occupation. This criterion also reflects the experience required for entry and the complexity of the responsibilities involved in the work, compared with other occupations.</dd><dt><strong>Skill requirements </strong>are<strong> </strong></dt><dd>specific abilities, aptitudes and/or knowledge that are prerequisites needed to obtain employment in an occupational group.</dd><dt><strong>Skill Type</strong></dt><dd>refers to a skill type represents the type of work performed and may be associated with a function (e.g. management, clerical or sales), a field (e.g. science, health, education or culture) or an industry (primary industry or manufacturing).</dd><dt><strong>Source population </strong>are<strong> </strong></dt><dd>individuals aged 15 years and over</dd><dt><strong>Trade/vocational </strong></dt><dd>is a level of education that may or may not require the completion of high school and may involve on-the-job training as part of the course requirements.</dd><dt><strong>Tradesperson </strong></dt><dd>is a skilled manual worker in a particular trade or craft.</dd><dt><strong>Trend </strong></dt><dd>refers to continues tendency or behaviour of a set of observations.</dd><dt><strong>Unemployment</strong></dt><dd>refers to the number of persons 15 years and over who were available but without work and actively looked for it in any given year. It is the difference between the labour force and employment.</dd><dt><strong>Unemployment rate</strong></dt><dd>is the number of unemployed persons expressed as a percentage of the labour force.</dd></dl><dl></dl>
<!-- part 4 start -->
	</div></div>
	<!-- Main content ends / Fin du contenu principal --> 
<!-- part 4 end -->
<!-- part 5 starts -->
	<!-- Primary navigation (left column) begins / D�but de la navigation principale (colonne gauche) -->
	<div id="cn-left-col-gap"></div>
	<div id="cn-left-col"><div id="cn-left-col-inner">
		<h2 id="cn-nav">Primary navigation (left column)</h2>
		<div class="cn-left-col-default">
<!-- clf2-nsi2 theme begins / D�but du th�me clf2-nsi2 -->
<!-- part 5 ends -->
    <h3><a href="w.2lc.4me@-eng.jsp">Canadian Occupational Projection System</a></h3>
    <ul>
      <li class="exploreoursite">
        <a href="c.4nt.2nt@-eng.jsp?cid=3">About the Projections</a>
      </li>
      <li class="exploreoursite">
      	<a href="pr.4j.2ct.3ons.5mm.1ries@-eng.jsp">Projection Summaries</a>
      </li>
      <li class="exploreoursite">
        <a href="pr.4j.2ct.3ond.1ta@-eng.jsp">Projection Data</a>
      </li>
      <li class="exploreoursite">
        <a href="l.3bd.2t.1ils@-eng.jsp">Information Documents</a>
      </li>
      <li class="exploreoursite">
        <a href="gl.4ss.1ry@-eng.jsp">Glossary</a>
      </li>
      <li class="exploreoursite">
        <a href="http://www5.hrsdc.gc.ca/NOC/English/NOC/2006/Welcome.aspx">National Occupation Classification (<acronym title="National Occupation Classification">NOC</acronym>)</a>
      </li>
      <li class="exploreoursite">
      <a href="http://www.workingincanada.gc.ca">Job Bank</a>
      </li>
    </ul>
<br />
<!-- part 6 starts -->
<!-- clf2-nsi2 theme ends / Fin du th�me clf2-nsi2 -->
<!-- clf2-nsi2 theme begins / D�but du th�me clf2-nsi2 -->
				<h3><a href="http://www.hrsdc.gc.ca/eng/home.shtml">Department</a></h3>
				<ul>
				<li><a href="http://www.hrsdc.gc.ca/eng/corporate/ministers/index.shtml">Ministers</a></li>
				<li><a href="http://www.hrsdc.gc.ca/eng/corporate/about_us/index.shtml">About Us</a></li>
				<li><a href="http://www.hrsdc.gc.ca/eng/corporate/newsroom/index.shtml">Newsroom</a></li>
				<li><a href="http://www.hrsdc.gc.ca/eng/publications_resources/index.shtml">Publications and Resources</a></li>
				<li><a href="http://www.hrsdc.gc.ca/eng/funding_programs/index.shtml" >Funding Programs</a></li>
				<li><a href="http://www.hrsdc.gc.ca/eng/consultations/index.shtml" >Consultation and Engagement</a></li>
				<li><a href="http://www.hrsdc.gc.ca/eng/success_stories/index.shtml">Success Stories</a></li>
				<li><a href="http://www.hrsdc.gc.ca/eng/labour/index.shtml">Labour Program</a></li>
				<li><a href="http://www.servicecanada.gc.ca/eng/home.shtml" >Service Canada</a></li>
				</ul>		
				<h3>Explore the site</h3>
				<ul class="cn-explore-site">
					<li><a href="http://www.hrsdc.gc.ca/eng/corporate/topics/index.shtml">Topics</a></li>
					<li><a href="http://www.hrsdc.gc.ca/eng/corporate/policy_programs/index.shtml">Programs and Policies</a></li>
					<li><a href="http://www.hrsdc.gc.ca/eng/corporate/az/index.shtml">A to Z Index</a></li>
				</ul>
				<h3>Transparency</h3>
				<ul>
					<li><a href="http://www.hrsdc.gc.ca/eng/access_information/atirequest/index.shtml">Completed Access to Information Requests</a></li>
					<li><a href="http://www.hrsdc.gc.ca/eng/corporate/proactive_disclosure/index.shtml">Proactive Disclosure</a></li>
				</ul>
<!-- clf2-nsi2 theme ends / Fin du th�me clf2-nsi2 -->
		</div>
	</div></div>
	<!-- Primary navigation (left column) ends / Fin de la navigation principale (colonne gauche) -->
<!-- part 6 ends -->
<script type='text/javascript'>
// <![CDATA[
  resetHeight(200);
// ]]>
</script>  
<!-- part 9 starts -->
	</div>
	<!-- Footer begins / D�but du pied de page --> 
	<div id="cn-foot"><div id="cn-foot-inner">
		<h2>Footer</h2>
<!-- clf2-nsi2 theme begins / D�but du th�me clf2-nsi2 -->
		<div id="cn-in-pd">			
			<dl id="cn-doc-dates">
				<dt>Date Modified:</dt>
				<dd><span>2015-02-21</span></dd>
			</dl>
			<div id="cn-toppage-foot"><a href="#cn-tphp" title="Return to Top of Page">Top of Page</a></div>
			<div id="cn-in-pd-links">
				<ul>
					<li id="cn-inotices-link"><a href="http://www.hrsdc.gc.ca/eng/corporate/important_notices/index.shtml" rel="license">Important Notices</a></li>
				</ul>
			</div>
		</div>
<!-- clf2-nsi2 theme ends / Fin du th�me clf2-nsi2 -->
	</div></div>
	<!-- Footer ends / Fin du pied de page -->
</div>
<!-- Three column layout ends / Fin de la mis en page de trois colonnes -->
</body>
<!-- part 9 ends -->
</html>
