﻿using log4net;
using RightCareer.Models;
using RightCareer.Utility;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RightCareer.Controllers
{
    public class CareerController : ApiController
    {
        private Repository repo = new Repository();
        private static Dictionary<CacheKeys, int> SimpleCache = new Dictionary<CacheKeys, int>();
        private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static CareerController()
        {
            SimpleCache.Add(CacheKeys.MAX_ITEMS_PERPAGE, AppSettings.getPhoneMaxPageSize());
        }
        /// <summary>
        /// Gets the list of career ordered by rank determined by the projected  surplus in future and wages
        /// </summary>
        /// <param name="preference"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("GetCareers")]
        public CareersOut GetCareers([FromUri]CareerPreference preference)
        {
            if (!ModelState.IsValid)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, this.ModelState));
            }

            IList<RightCareerChoice> bestCareersList = new List<RightCareerChoice>();
            if (preference != null)
            {
                IList<NOCWage> preferCareers = repo.GetCareersWithPreference(preference);

                foreach (NOCWage wage in preferCareers)
                {
                    NOC noc = repo.GetNOCFromCode(wage.NOC_CODE);

                    if (noc != null)
                    {
                        NOCProjection nocProjection = repo.GetNOCProjectionFromNOC(noc);

                        if (nocProjection != null)
                        {
                            Province province = repo.GetProvinceFromCode(preference);
                            string finalAssessment = nocProjection.Final_Assessment_Eng;

                            int assessmentRank = CareerUtility.GetAssessmentRank(finalAssessment);
                            decimal? bestCareers = (wage.AvgWageNSalaries * assessmentRank);

                            RightCareerChoice theChoice = new RightCareerChoice();
                            theChoice.NOCCODE = noc.Code;
                            theChoice.NOCDescription = string.IsNullOrEmpty(noc.Description) ? "Occupation not available" : noc.Description;
                            theChoice.score = bestCareers.Value;

                            if (province != null)
                            {
                                theChoice.provincecode = province.ProvinceCode;
                                theChoice.provincename = province.ProvinceName;
                            }
                            else
                            {
                                theChoice.provincecode = preference.provincecode;
                            }


                            bestCareersList.Add(theChoice);
                        }
                        else
                        {
                            logger.Warn("No projection foud for NOC " + noc.Code + " obtained from NOC table");
                        }
                    }
                    else
                    {
                        logger.Warn("No NOC foud for code " + wage.NOC_CODE + " obtained from NOCWages table");
                    }
                }
            }


            int offset = 0;

            if (!int.TryParse(preference.offset, out offset))
            {
                logger.Warn("The provided offset " + preference.offset + " is not in correct format. Defaulting start index  to" + offset);
            }

            int maxItemsPerpage = GetMaxItemsPerPage();
            int starIndex = (offset * maxItemsPerpage);
            IList<RightCareerChoice> sortedCareersList = bestCareersList
                                                            .OrderByDescending(r => r.score)
                                                            .Skip(starIndex)
                                                            .Take(maxItemsPerpage)
                                                            .ToList();
            var careersOut = new CareersOut();
            careersOut.TotalCount = bestCareersList.Count;
            careersOut.CareersList = sortedCareersList;

            return careersOut;
        }

        [HttpGet]
        [ActionName("GetCareerStats")]
        public CareerStats GetCareerStats([FromUri]CareerStatsPref statsPref)
        {
            if (!ModelState.IsValid)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, this.ModelState));
            }
            CareerStats stats = new CareerStats();
            stats.NocCode = statsPref.noccode;
            stats.NocDescription = statsPref.nocdescription;
            stats.ProvinceName = statsPref.provincename;

            NOC noc = repo.GetNOCFromCode(statsPref.noccode);

            if (noc != null)
            {
                NOCProjection nocProjection = repo.GetNOCProjectionFromNOC(noc);

                if (nocProjection != null)
                {
                    stats.FinalAssesment = nocProjection.Final_Assessment_Eng;

                }
                else
                {
                    logger.Warn("No projection foud for NOC " + noc.Code + " obtained from NOC table");
                }
            }
            else
            {
                logger.Warn("No NOC foud for code " + statsPref.noccode + " obtained from NOCWages table");
            }
            return stats;
        }

        [HttpGet]
        [ActionName("GetSortedCareers")]
        public CareersOut GetSortedCareers([FromUri]SortingPref sortingPref)
        {
            short defaultSortType = (short)SortBy.AVERAGEWAGE;

            if (!short.TryParse(sortingPref.sortby, out defaultSortType))
            {
                logger.Warn("Could not parse sort preference " + sortingPref.sortby + ", Defaulting to AVERAGEWAGE");
            }
            byte defaulSortMethod = (byte)SortOrder.DESC;

            if (!byte.TryParse(sortingPref.sortorder, out defaulSortMethod))
            {
                logger.Warn("Could not parse sort order " + sortingPref.sortorder + ", Defaulting to DESC");
            }

            SortBy sortpref = (SortBy)defaultSortType;
            SortOrder orderPref = (SortOrder)defaulSortMethod;

            IList<RightCareerChoice> bestCareersList = new List<RightCareerChoice>();
            Province province = repo.GetProvinceFromCode(sortingPref);
            switch (sortpref)
            {
                case SortBy.NOCCODE:
                    IList<NOC> nocSorted = repo.GetSortedCareerByNOC(orderPref, province);
                    bestCareersList = GetRightCareerChoiceList(nocSorted, province, sortingPref);
                    break;
                case SortBy.MEDIANWAGE:
                    IList<NOCWage> wageSorted = repo.GetSortedCareerByMedianWage(orderPref, province);
                    bestCareersList = GetRightCareerChoiceList(wageSorted,province, sortingPref);
                    break;
                case SortBy.AVERAGEWAGE:
                default:
                    IList<NOCWage> avgSorted = repo.GetSortedCareerByAverageWage(orderPref, province);
                    bestCareersList = GetRightCareerChoiceList(avgSorted,province, sortingPref);
                    break;
            }

            int offset = 0;

            if (!int.TryParse(sortingPref.offset, out offset))
            {
                logger.Warn("The provided offset " + sortingPref.offset + " is not in correct format. Defaulting start index  to" + offset);
            }

            int maxItemsPerpage = GetMaxItemsPerPage();
            int starIndex = (offset * maxItemsPerpage);


            IList<RightCareerChoice> sortedCareersList = bestCareersList
                                                           .OrderByDescending(r => r.score)
                                                           .Skip(starIndex)
                                                           .Take(maxItemsPerpage)
                                                           .ToList();


            var careersOut = new CareersOut();
            careersOut.TotalCount = bestCareersList.Count;
            careersOut.CareersList = sortedCareersList;

            return careersOut;

        }

        private IList<RightCareerChoice> GetRightCareerChoiceList(IList<NOC> nocSorted, Province province, SortingPref sortingPref)
        {
            IList<RightCareerChoice> bestCareersList = new List<RightCareerChoice>();

            if (nocSorted != null)
            {
                foreach (NOC noc in nocSorted)
                {
                    RightCareerChoice theChoice = new RightCareerChoice();

                    theChoice.NOCCODE = noc.Code;
                    theChoice.NOCDescription = string.IsNullOrEmpty(noc.Description) ? "Occupation not available" : noc.Description;

                    if (province != null)
                    {
                        theChoice.provincecode = province.ProvinceCode;
                        theChoice.provincename = province.ProvinceName;
                    }
                    else
                    {
                        theChoice.provincecode = sortingPref.provincecode;
                    }

                    bestCareersList.Add(theChoice);
                }

            }

            return bestCareersList;

        }

        /// <summary>
        /// Return RightCareerChoic list from NOCWage list
        /// </summary>
        /// <param name="wageSorted"></param>
        /// <param name="province"></param>
        /// <param name="sortingPref"></param>
        /// <returns></returns>
        private IList<RightCareerChoice> GetRightCareerChoiceList(IList<NOCWage> wageSorted, Province province, SortingPref sortingPref)
        {
            IList<RightCareerChoice> bestCareersList = new List<RightCareerChoice>();

            if (wageSorted != null)
            {
                foreach (NOCWage wage in wageSorted)
                {
                    RightCareerChoice theChoice = new RightCareerChoice();
                    NOC noc = repo.GetNOCFromCode(wage.NOC_CODE);
                    theChoice.NOCCODE = noc.Code;
                    theChoice.NOCDescription = string.IsNullOrEmpty(noc.Description) ? "Occupation not available" : noc.Description;

                    if (province != null)
                    {
                        theChoice.provincecode = province.ProvinceCode;
                        theChoice.provincename = province.ProvinceName;
                    }
                    else
                    {
                        theChoice.provincecode = sortingPref.provincecode;
                    }

                    bestCareersList.Add(theChoice);
                }

            }

            return bestCareersList;
        }

        private static short GetMaxItemsPerPage()
        {
            if (SimpleCache.ContainsKey(CacheKeys.MAX_ITEMS_PERPAGE))
            {
                return (short)SimpleCache[CacheKeys.MAX_ITEMS_PERPAGE];
            }
            else
            {
                return AppSettings.getPhoneMaxPageSize();
            }
        }
    }

    enum CacheKeys
    {
        MAX_ITEMS_PERPAGE
    }
}
