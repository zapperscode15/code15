﻿using Newtonsoft.Json.Serialization;
using RightCareer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Data.Entity;

namespace RightCareer.Controllers
{
    public class HomeController : ApiController
    {
        private Repository repo = new Repository();

        /// <summary>
        /// Get list of provinces
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName("GetProvinces")]
        public IEnumerable<Province> GetProvinces()
        {
            return repo.GetProvinces().AsEnumerable();
        }

        /// <summary>
        /// Get List of education types
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName("GetEducationTypes")]
        public IEnumerable<EducationType> GetEducationTypes()
        {
            return repo.GetEducationTypes().AsEnumerable();
        }

        [HttpGet]
        [ActionName("GetAllNOC")]
        public IEnumerable<NOC> GetNOCS()
        {
            return repo.GetNOCs().AsEnumerable();
        }


    }
}
