﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RightCareer.Models
{
    public class CareerPreference
    {
        public string provincecode
        {
            get;
            set;
        }

        public string educationcode
        {
            get;
            set;
        }

        public string offset
        {
            get;
            set;
        }
    }
}