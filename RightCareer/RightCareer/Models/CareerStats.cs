﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RightCareer.Models
{
    public class CareerStats
    {
        public string NocCode
        {
            get;
            set;
        }

        public string NocDescription
        {
            get;
            set;
        }

        public decimal MedianSalary
        {
            get;
            set;
        }

        public decimal AverageSalary
        {
            get;
            set;
        }

        public double AggregateProjection
        {
            get;
            set;
        }

        public string FinalAssesment
        {
            get;
            set;
        }

        public string ProvinceName
        {
            get;
            set;
        }
    }
}