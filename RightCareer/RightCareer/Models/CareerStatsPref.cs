﻿
namespace RightCareer.Models
{
    public class CareerStatsPref
    {
        public string provincename
        {
            get;
            set;
        }

        public string provincecode
        {
            get;
            set;
        }

        public string noccode
        {
            get;
            set;
        }

        public string nocdescription
        {
            get;
            set;
        }
    }
}