﻿using System.Collections.Generic;

namespace RightCareer.Models
{
    public class CareersOut
    {

        public int TotalCount
        {
            get;
            set;
        }

        public IList<RightCareerChoice> CareersList
        {
            get;
            set;
        }
    }
}