﻿using CsvHelper;
using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RightCareer.Models
{
    /// <summary>
    /// Class to contruct NOCWageInfo from csv file line
    /// </summary>
    public sealed class NOCWageInfo
    {
        public string OccupationWithCode;
        public Decimal Total_Employment_Income_Statistics;
        public Decimal With_Employment_Income;
        public Decimal Median_Employment_Income;
        public Decimal Average_Employment_Income;
        public Decimal Wages_and_salaries;
        public Decimal Median_Wage_And_Salaries;
        public Decimal Average_Wages_And_salaries;

        public NOCWageInfo GetNOCWageInfoFromCSV(CsvReader csvreader)
        {
            this.OccupationWithCode = csvreader.GetField<string>(0).Trim();
            this.Total_Employment_Income_Statistics = csvreader.GetField<Decimal>(1);
            this.With_Employment_Income = csvreader.GetField<Decimal>(2);
            this.Median_Employment_Income = csvreader.GetField<Decimal>(3);
            this.Average_Employment_Income = csvreader.GetField<Decimal>(4);
            this.Wages_and_salaries = csvreader.GetField<Decimal>(5);
            this.Median_Wage_And_Salaries = csvreader.GetField<Decimal>(6);
            this.Average_Wages_And_salaries = csvreader.GetField<Decimal>(7);

            return this;

        }
    }

    /// <summary>
    /// Class to construct NOCProjection object from csv file line
    /// </summary>
    public sealed class NOCProjectionsInfo
    {
        public NOCProjection GetNOCProjectionsFromCSV(CsvReader csvreader)
        {
            NOCProjection projection = new NOCProjection();

            projection.NOC_CODE = csvreader.GetField<string>(0).Trim();
            projection.Employment_2012 = csvreader.GetField<double>(1);
            projection.Cumiliative_Emp_Growth_2013_2022 = csvreader.GetField<double>(2);
            projection.Cumulative_Retirements_2013_2022 = csvreader.GetField<double>(3);
            projection.Cumulative_Other_Replacement_Demand_2013_2022 = csvreader.GetField<double>(4);
            projection.Cumulative_JobOpenings_2013_2022_ = csvreader.GetField<double>(5);
            projection.Cumulative_School_Leavers_2013_2022_ = csvreader.GetField<double>(6);
            projection.Cumulative_Immigrants_In_Market_2013_2022 = csvreader.GetField<double>(7);
            projection.Cumulative_Jobseekers_Other_2013_2022_ = csvreader.GetField<double>(8);
            projection.Cumulative__Jobseekers_2013_2022 = csvreader.GetField<double>(9);
            projection.Assessment_Labourmarket_2010_2012_Eng = csvreader.GetField<string>(10).Trim();
            projection.Assessment_Labourmarket_2010_2012_Fr = csvreader.GetField<string>(11).Trim();
            projection.Final_Assessment_Eng = csvreader.GetField<string>(12).Trim();
            projection.Final_Assessment_Fr_ = csvreader.GetField<string>(13).Trim();

            return projection;
        }

    }
}