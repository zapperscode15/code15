﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RightCareer.Models
{
    /// <summary>
    /// Class to hold final assessments for projections
    /// </summary>
    public enum ProjectionAssessments
    {
        BALANCE=1,
        SHORTAGE=2,
        SURPLUS=-1,
    }
}