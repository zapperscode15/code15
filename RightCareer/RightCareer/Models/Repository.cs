﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RightCareer.Models
{
    public class Repository
    {
        private Province provinces;

        /// <summary>
        /// returns all the provinces
        /// </summary>
        /// <returns></returns>
        internal IList<Province> GetProvinces()
        {
            using (RightCareerDataContext db = this.GetDatabaseContext())
            {
                var provinces = (from p in db.Provinces select p);
                return provinces.ToList();
            }

        }

        internal IList<EducationType> GetEducationTypes()
        {
            using (RightCareerDataContext db = this.GetDatabaseContext())
            {
                return db.EducationTypes.ToList();
            }

        }

        // Returns all the NOCS
        internal IList<NOC> GetNOCs()
        {
            using (RightCareerDataContext db = this.GetDatabaseContext())
            {
                var nocs = from n in db.NOCs select n;
                return nocs.ToList();
            }
        }

        /// <summary>
        /// Gets all the edication types
        /// </summary>
        /// <returns></returns>

        internal IList<EducationType> GeEducationTypes()
        {
            using (RightCareerDataContext db = this.GetDatabaseContext())
            {
                var eduTypes = from e in db.EducationTypes select e;
                return eduTypes.ToList();
            }

        }

        /// <summary>
        /// Save changes to NOCWages table
        /// </summary>
        /// <param name="wageInfo">NOCWageInfo instance</param>
        /// <param name="certificate">EducationType for education code</param>
        /// <param name="nocCode">NOC instance for noc code</param>
        internal void SaveNOCWage(NOCWageInfo wageInfo, EducationType certificate, Province province, RightCareerDataContext db)
        {
            NOC nocCode = this.getNocFromInfo(wageInfo);

            NOCWage nocWage = this.getNocWageFromInfo(wageInfo, certificate, nocCode, province);

            db.NOCWages.InsertOnSubmit(nocWage);


        }

        internal NOC getNocFromInfo(NOCWageInfo nocInfo)
        {
            char[] sep = { ' ' };
            string[] nocCode = nocInfo.OccupationWithCode.Split(sep, 2, StringSplitOptions.None);

            NOC noc = new NOC();
            noc.Code = nocCode[0];
            noc.Description = nocCode[1];

            return noc;
        }
        private NOCWage getNocWageFromInfo(NOCWageInfo wageInfo, EducationType certificate, NOC nocCode, Province province)
        {
            NOCWage wage = new NOCWage();
            wage.Education_Type = certificate.Id;
            wage.NOC_CODE = nocCode.Code;
            wage.EmploymentIncome = wageInfo.With_Employment_Income;
            wage.MedianEmployment = wageInfo.Median_Employment_Income;
            wage.AvgEmployment = wageInfo.Average_Employment_Income;
            wage.WageNSalaries = wageInfo.Wages_and_salaries;
            wage.MedianWageNSalaries = wageInfo.Median_Wage_And_Salaries;
            wage.AvgWageNSalaries = wageInfo.Average_Wages_And_salaries;
            wage.ProvinceCode = province.ProvinceCode;

            return wage;
        }

        // truncates the NOCWages table
        internal void ClearNOCWage()
        {
            using (RightCareerDataContext db = this.GetDatabaseContext())
            {
                db.ExecuteCommand("TRUNCATE TABLE NOCWages");
            }
        }

        // truncates the NOCProjections table
        internal void ClearNOCProjection()
        {
            using (RightCareerDataContext db = this.GetDatabaseContext())
            {
                db.ExecuteCommand("TRUNCATE TABLE NOCProjections");
            }
        }

        // Delete contents of the NOC table
        internal void ClearNOC()
        {
            using (RightCareerDataContext db = this.GetDatabaseContext())
            {
                db.ExecuteCommand("DELETE FROM NOC");
            }
        }

        // save NOCProjection table
        internal void SaveNOCProjections(NOCProjection projections, RightCareerDataContext db)
        {
            db.NOCProjections.InsertOnSubmit(projections);

            db.SubmitChanges();
        }

        // save NOC if not present
        internal void SaveNOC(NOC noc, RightCareerDataContext db)
        {
            lock (NOC_LOCK)
            {

                IList<NOC> allNocs = new Repository().GetNOCs();
                NOC existing = this.GetNOCFromCode(noc.Code);

                if (existing == null)
                {
                    db.NOCs.InsertOnSubmit(noc);
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(noc.Description))
                    {
                        existing = noc;
                    }
                }

                db.SubmitChanges();
            }
        }

        internal RightCareerDataContext GetDatabaseContext()
        {
            RightCareerDataContext db = new RightCareerDataContext();
            db.DeferredLoadingEnabled = true;

            return db;
        }

        private static Object NOC_LOCK = new Object();

        /// <summary>
        /// Returns the craeer ordered deceding based on avg wage
        /// </summary>
        /// <param name="preference"></param>
        /// <returns></returns>
        internal IList<NOCWage> GetCareersWithPreference(CareerPreference preference)
        {
            using (RightCareerDataContext db = this.GetDatabaseContext())
            {
                EducationType education = db.EducationTypes.
                    Where(e => e.Code.Equals(preference.educationcode)).Single();
                IList<NOCWage> careerWages = db.NOCWages.
                Where(n => n.Education_Type.Equals(education.Id) && n.ProvinceCode.Equals(preference.provincecode))
                .OrderByDescending(n => n.AvgWageNSalaries)
                .ToList();

                return careerWages;
            }
        }

        internal NOCProjection GetNOCProjectionFromNOC(NOC noc)
        {
            using (RightCareerDataContext db = this.GetDatabaseContext())
            {
                NOCProjection projection = db.NOCProjections.Where(p => p.NOC_CODE.Equals(noc.Code)).SingleOrDefault();

                return projection;
            }
        }

        internal NOC GetNOCFromCode(string noccode)
        {
            using (RightCareerDataContext db = this.GetDatabaseContext())
            {
                NOC noc = db.NOCs.Where(no => no.Code.Equals(noccode)).SingleOrDefault();

                return noc;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="preference"></param>
        /// <returns></returns>
        internal Province GetProvinceFromCode(CareerPreference preference)
        {

            using (RightCareerDataContext db = this.GetDatabaseContext())
            {
                Province province = db.Provinces.Where(p => p.ProvinceCode.Equals(preference.provincecode)).SingleOrDefault();

                return province;
            }
        }

        internal Province GetProvinceFromCode(SortingPref preference)
        {

            using (RightCareerDataContext db = this.GetDatabaseContext())
            {
                Province province = db.Provinces.Where(p => p.ProvinceCode.Equals(preference.provincecode)).SingleOrDefault();

                return province;
            }
        }

        internal IList<NOC> GetSortedCareerByNOC(SortOrder orderPref,Province province)
        {
            using (RightCareerDataContext db = this.GetDatabaseContext())
            {
                switch(orderPref)
                {
                    case SortOrder.DESC:
                         return (from noc in db.NOCs join wage in db.NOCWages on noc.Code equals wage.NOC_CODE 
                                                    where wage.ProvinceCode.Equals(province.ProvinceCode)  
                                                    orderby noc.Description descending 
                                                    select noc).ToList();
                        
                    case SortOrder.ASC:
                    default:
                        return (from noc in db.NOCs join wage in db.NOCWages on noc.Code equals wage.NOC_CODE 
                                                    where wage.ProvinceCode.Equals(province.ProvinceCode)  
                                                    orderby noc.Description ascending 
                                                    select noc).ToList();
                }                
            }
        }

        /// <summary>
        /// Sort by Median wage and Salary for a province
        /// </summary>
        /// <param name="orderPref"></param>
        /// <returns></returns>
        internal IList<NOCWage> GetSortedCareerByMedianWage(SortOrder orderPref, Province province)
        {
            using (RightCareerDataContext db = this.GetDatabaseContext())
            {

                switch (orderPref)
                {
                    case SortOrder.DESC:
                        return db.NOCWages.Where(n=>n.ProvinceCode.Equals(province.ProvinceCode)).OrderByDescending(n => n.MedianWageNSalaries).ToList();
                    case SortOrder.ASC:
                    default:
                        return db.NOCWages.Where(n=>n.ProvinceCode.Equals(province.ProvinceCode)).OrderBy(n => n.MedianWageNSalaries).ToList();
                }

            }
        }

        /// <summary>
        /// Sort by AverageWage and Salary for a province
        /// </summary>
        /// <param name="orderPref"></param>
        /// <returns></returns>
        internal IList<NOCWage> GetSortedCareerByAverageWage(SortOrder orderPref,  Province province)
        {
            using (RightCareerDataContext db = this.GetDatabaseContext())
            {

                switch (orderPref)
                {
                    case SortOrder.DESC:
                        return db.NOCWages.Where(n=>n.ProvinceCode.Equals(province.ProvinceCode)).OrderByDescending(n => n.AvgWageNSalaries).ToList();
                    case SortOrder.ASC:
                    default:
                        return db.NOCWages.Where(n=>n.ProvinceCode.Equals(province.ProvinceCode)).OrderBy(n => n.AvgWageNSalaries).ToList();
                }
            }
        }
    }
}