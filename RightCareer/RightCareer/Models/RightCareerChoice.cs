﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RightCareer.Models
{
    public class RightCareerChoice
    {
        public string NOCCODE
        {
            get;
            set;
        }
        public string NOCDescription
        {
            get;
            set;
        }
        public decimal score
        {
            get;
            set;
        }

        public string provincecode
        {
            get;
            set;
        }

        public string provincename
        {
            get;
            set;
        }
    }
}