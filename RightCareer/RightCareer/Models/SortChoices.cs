﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RightCareer.Models
{
    public enum SortBy : short
    {
        NOCCODE = 0,
        MEDIANWAGE = 1,
        AVERAGEWAGE = 2
    }

    public enum SortOrder : byte
    {
        ASC = 0,
        DESC = 1
    }
}