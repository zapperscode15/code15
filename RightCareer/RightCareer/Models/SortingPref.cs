﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RightCareer.Models
{
    public class SortingPref
    {
        public string sortby
        {
            get;
            set;
        }

        public string sortorder
        {
            get;
            set;
        }

        public string offset
        {
            get;
            set;
        }

        public string provincecode
        {
            get;
            set;
        }


    }
}