﻿var rightCareer = angular.module("rightCareer", []);

rightCareer.controller("rightCareerListCtl", function ($scope,$http) {
    

    $http.get("api/home/GetProvinces").success(function (prov) {
        $scope.provinces = prov;
    });

    $http.get("api/home/GetEducationTypes").success(function (edu) {
        $scope.eduTypes = edu;
    });

    $http.get("/api/Career/GetCareerStats", { provincename: $scope.prov, provincecode: "ON", noccode: "N0314", nocdescription: "description of occupation" }).
        success(function (data) {
            $scope.items = data;
    });

    $scope.tempItems = $scope.items;

    $scope.filterRows = function () {
        /*$scope.tempItems = $scope.items.filter(function (item) {
            return (item.province === $scope.prov && item.education === $scope.education);
        });*/
        $.ajax({
            type: 'GET',
            url: '/api/Career/GetCareers',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: { provincecode: $scope.prov, educationcode: $scope.education, offset: "1" },
            success: function (data) {
                console.log(data);
                $.ajax({
                    type: 'GET',
                    url: '/api/Career/GetCareerStats',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    data: { provincename: "Ontario", provincecode: "ON", noccode: "NO_CERT", nocdescription: "description of occupation" },
                    success: function (data) {
                        console.log(data2)
                    },
                    error: function (data) { console.log(data) }
                });
            },
            error: function (data) { console.log(data) }
        });

        $scope.$apply();
    };
});

rightCareer.controller("rightCareerListCtlMobile", function ($scope, $http) {

    $scope.offset = "0";

    $http.get("/api/home/GetProvinces").success(function (prov) {
        $scope.provinces = prov;
    });

    $http.get("/api/home/GetEducationTypes").success(function (edu) {
        $scope.eduTypes = edu;
    });

    
    $scope.getNocDescription = function (item) {
        
        $.ajax({
            type: 'GET',
            url: '/api/Career/GetCareerStats',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: { provincename: item.provincename, provincecode: item.provincecode, noccode: item.NOCCODE, nocdescription: item.NOCDescription },
            success: function (data) {
                console.log(data);
                $scope.nocItem = data;
                $scope.nocItem.score = item.score;
            },
            error: function (data) {
                console.log(data);
            }
        });
    };

    $scope.filterRows = function () {

        $.ajax({
            type: 'GET',
            url: '/api/Career/GetCareers',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: { provincecode: $scope.prov, educationcode: $scope.education, offset: $scope.offset },
            success: function (data) {
                $scope.itemsMobile = data.CareersList;
                $scope.tempItemsMobile = $scope.itemsMobile;
                $scope.$apply();
            },
            error: function (data) {
                console.log(data);
            }
        });
        
    };

    var sortby;
    var sortorder;

    $scope.sortRows = function () {
        
        switch ($scope.querySort) {
            case "00":
                sortby = "0";
                sortorder = "0";
                break;
            case "01":
                sortby = "0";
                sortorder = "1";
                break;
            case "10":
                sortby = "1";
                sortorder = "0";
                break;
            case "11":
                sortby = "1";
                sortorder = "1";
                break;
            case "20":
                sortby = "2";
                sortorder = "0";
                break;
            case "21":
                sortby = "2";
                sortorder = "1";
                break;
        }

        $.ajax({
            type: 'GET',
            url: '/api/Career/GetSortedCareers',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: { sortby: sortby, sortorder: sortorder, offset: "0", provincecode: $scope.prov},
            success: function (data) {
                $scope.itemsMobile = data.CareersList;
                $scope.tempItemsMobile = $scope.itemsMobile;
                $scope.$apply();
            },
            error: function (data) {
                console.log(data);
            }
        });

    };
});