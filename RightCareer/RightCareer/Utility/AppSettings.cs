﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace RightCareer.Utility
{
    public static class AppSettings
    {
        private static Configuration rootWebConfig;
        private static readonly short defaultPageSize = 10;
        public static readonly short MAX_RECORDS_ToRETRIEVE = 30;

        static AppSettings()
        {
            rootWebConfig = WebConfigurationManager.OpenWebConfiguration("~/");
        }

        internal static short getPhoneMaxPageSize()
        {
            KeyValueConfigurationElement phoneMaxPage = rootWebConfig.AppSettings.Settings["PhoneMaxPageSize"];

            if (phoneMaxPage != null)
            {
                short normalPageSize = defaultPageSize;
                short.TryParse(phoneMaxPage.Value, out normalPageSize);

                return normalPageSize;
            }
            else
            {
                return defaultPageSize;
            }
        }

        internal static String getRootWagesDataDir()
        {
            KeyValueConfigurationElement phoneMaxPage = rootWebConfig.AppSettings.Settings["RootWagesDataDir"];
            if (phoneMaxPage != null)
            {
                return phoneMaxPage.Value;
            }
            else
            {
                return string.Empty;
            }
        }

        internal static bool ReloadDB()
        {
            KeyValueConfigurationElement phoneMaxPage = rootWebConfig.AppSettings.Settings["ReloadDatabase"];
            
            if (phoneMaxPage != null)
            {
                bool update = false;
                bool.TryParse(phoneMaxPage.Value, out update);
                
                return update;
            }
            else
            {
                return false;
            }
        }

        internal static bool ReloadNOCSTable()
        {
            KeyValueConfigurationElement phoneMaxPage = rootWebConfig.AppSettings.Settings["ReloadNOCS"];

            if (phoneMaxPage != null)
            {
                bool update = false;
                bool.TryParse(phoneMaxPage.Value, out update);

                return update;
            }
            else
            {
                return false;
            }
        }

        internal static bool ReloadWagesTable()
        {
            KeyValueConfigurationElement phoneMaxPage = rootWebConfig.AppSettings.Settings["ReloadWages"];

            if (phoneMaxPage != null)
            {
                bool update = false;
                bool.TryParse(phoneMaxPage.Value, out update);

                return update;
            }
            else
            {
                return false;
            }
        }

        internal static bool ReloadProjectionTable()
        {
            KeyValueConfigurationElement phoneMaxPage = rootWebConfig.AppSettings.Settings["ReloadProjections"];

            if (phoneMaxPage != null)
            {
                bool update = false;
                bool.TryParse(phoneMaxPage.Value, out update);

                return update;
            }
            else
            {
                return false;
            }
        }

        internal static string getSummaryProjectionsFilePath()
        {
            KeyValueConfigurationElement phoneMaxPage = rootWebConfig.AppSettings.Settings["SummaryProjectionsFilePath"];
            if (phoneMaxPage != null)
            {
                return phoneMaxPage.Value;
            }
            else
            {
                return string.Empty;
            }
        }
    }

    
}