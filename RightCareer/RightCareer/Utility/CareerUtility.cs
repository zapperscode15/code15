﻿using RightCareer.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace RightCareer.Utility
{
    public class CareerUtility
    {

        public static Stream GetDataStreamFromFile(string filePath)
        {
            FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);

            return fileStream;
        }

        /// <summary>
        /// Check if a given line start with teh 4 digit number which is standard NOC code.
        /// </summary>
        /// <param name="line">line to check</param>
        /// <returns>trus ifstart with 4 digits followed by space, false otherwise</returns>
        public static bool isMatchNOCCode(string line)
        {
            if (!string.IsNullOrWhiteSpace(line))
            {
                if (Regex.IsMatch(line, @"^\d{4}?\s"))
                {
                    return true;
                }
            }

            return false;

        }


        public static T EnumFromString<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value);
        }

        internal static int GetAssessmentRank(string finalAssessment)
        {
            ProjectionAssessments assesment = EnumFromString(finalAssessment);

            
            return (int)assesment;
        }
        internal static ProjectionAssessments EnumFromString(string finalAssessment)
        {
            return (ProjectionAssessments)Enum.Parse(typeof(ProjectionAssessments), finalAssessment);
        }
    }
}