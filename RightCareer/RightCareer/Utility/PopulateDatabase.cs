﻿using CsvHelper;
using log4net;
using RightCareer.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Transactions;

namespace RightCareer.Utility
{
    public class PopulateDatabase
    {
        private static Repository repo;
        private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static PopulateDatabase()
        {
            repo = new Repository();
        }

        /// <summary>
        /// Initialize the database with data if reload db is true in settings.
        /// </summary>
        public static void InitDatabase()
        {
            try
            {
                // these should be in this order
                // clear tables first and then repopulate them

                if (AppSettings.ReloadNOCSTable())
                {
                    repo.ClearNOC();
                }

                if (AppSettings.ReloadWagesTable())
                {
                    repo.ClearNOCWage();

                    // do async operations
                    Action nocWagesAction = PupulateNOCWages;
                    AsyncCallback nocWagesActionActionCallback = new AsyncCallback(NOCWagesActionActionCompletedCallback);
                    nocWagesAction.BeginInvoke(nocWagesActionActionCallback, null);
                }

                if (AppSettings.ReloadProjectionTable())
                {
                    repo.ClearNOCProjection();

                    Action nocProjectionAction = PupulateNOCProjections;
                    AsyncCallback nocProjectionActionCallback = new AsyncCallback(NOCProjectionActionCompletedCallback);
                    nocProjectionAction.BeginInvoke(nocProjectionActionCallback, null);
                }
            }
            catch (Exception ex)
            {
                logger.Error("Unable to populate database " + ex);
            }

        }

        //call back
        private static void NOCWagesActionActionCompletedCallback(IAsyncResult ar)
        {
            logger.Info("========Completed pupulating  NOCWages table===========");
        }

        // call back
        private static void NOCProjectionActionCompletedCallback(IAsyncResult ar)
        {
            logger.Info("========Completed pupulating  NOCProjections table===========");
        }

        // populate the noc summary projection table
        private static void PupulateNOCProjections()
        {
            logger.Info("========Started populating NOCProjections========");

            string summaryFilePath = AppSettings.getSummaryProjectionsFilePath();

            using (var reader = new CsvReader(new StreamReader(CareerUtility.GetDataStreamFromFile(summaryFilePath))))
            {
                using (var context = repo.GetDatabaseContext())
                {
                    while (reader.Read())
                    {
                        try
                        {
                            // save here
                            using (var transaction = new TransactionScope())
                            {
                                NOCProjection projections = new NOCProjectionsInfo().GetNOCProjectionsFromCSV(reader);
                                NOC noc = new NOC();
                                noc.Code = projections.NOC_CODE;
                                noc.Description = string.Empty;

                                //save noc code
                                repo.SaveNOC(noc, context);

                                //save noc projections
                                repo.SaveNOCProjections(projections, context);
                                context.SubmitChanges();

                                transaction.Complete();

                                logger.Debug("Saved NOC with code" + noc.Code + " and noc projection");
                            }

                        }
                        catch (Exception ex)
                        {
                            logger.Error("Unable to save the with noce code " + reader.GetField<string>(0), ex);
                        }
                    }
                }
            }

            logger.Info("Completed parsing file" + summaryFilePath);
        }

        // populate wages table
        private static void PupulateNOCWages()
        {
            logger.Info("=====Started populating NOCWages========");

            string wagesRootDir = AppSettings.getRootWagesDataDir();
            using (var contecxt = repo.GetDatabaseContext())
            {
                IList<Province> allProvinces = repo.GetProvinces();
                IList<EducationType> allEducations = repo.GeEducationTypes();
                int rowsInserted = 1;
                // populate for each province and each certificate read file and populate
                foreach (Province pr in allProvinces)
                {
                    foreach (EducationType certificateType in allEducations)
                    {
                        string wageFile = getProvinceWageDataPath(wagesRootDir, pr, certificateType);

                        // do this for each line.                        
                        // only lines statring with 4 digit NOC code are our interest 

                        // now initialize the CsvReader
                        using (var csvReader = new CsvReader(new StreamReader(CareerUtility.GetDataStreamFromFile(wageFile))))
                        {
                            while (csvReader.Read())
                            {
                                string wageField = csvReader.GetField<string>(0).Trim();

                                if (CareerUtility.isMatchNOCCode(wageField))
                                {

                                    // get the digits and description                               
                                    NOCWageInfo nocWageInfo = new NOCWageInfo().GetNOCWageInfoFromCSV(csvReader);

                                    try
                                    {

                                        using (var transaction = new TransactionScope())
                                        {
                                            NOC nocCode = repo.getNocFromInfo(nocWageInfo);
                                            //save noc code
                                            repo.SaveNOC(nocCode, contecxt);
                                            //save noc wage
                                            repo.SaveNOCWage(nocWageInfo, certificateType, pr, contecxt);

                                            contecxt.SubmitChanges();

                                            transaction.Complete();

                                            logger.Debug("Inserted NOC and NocWage information. Number of rows inserted = " + rowsInserted);
                                        }

                                        rowsInserted++;
                                    }
                                    catch (Exception ex)
                                    {
                                        logger.Error("Unable to save the line " + wageField + " in file " + wageFile, ex);
                                    }
                                }
                            }
                        }

                        logger.Info("Completed parsing file" + wageFile);
                    }
                }
            }
        }

        private static string getProvinceWageDataPath(string rootDir, Province province, EducationType certificateType)
        {
            string provinceDirPath = Path.Combine(rootDir, province.ProvinceCode);
            string completePath = Path.Combine(provinceDirPath, getFileName(province, certificateType));

            return completePath;
        }

        private static string getFileName(Province province, EducationType certificateType)
        {
            string filename = string.Format("Wages_{0}_{1}.csv", province.ProvinceCode, getFileSuffix(certificateType));

            return filename;
        }

        private static string getFileSuffix(EducationType certificateType)
        {
            switch (certificateType.Code.Trim())
            {
                case "NO_CERT":
                    return "NoDegreeDiploma";
                case "HIGHSCHOOL_EQU":
                    return "HighSchoolDiploma";
                case "POST_SECONDARY":
                    return "PostsecondaryDegreeDiploma";
                case "POST_SECONDAR_BELOW_BACHELORS":
                    return "PostsecondaryDegreeBelowBachelors";
                case "UNIVERSITY_CERTIFICATE":
                    return "UniveristyDegree";
                default:
                    return string.Empty;
            }
        }
    }
}